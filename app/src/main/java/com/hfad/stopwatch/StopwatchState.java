package com.hfad.stopwatch;

/**
 * Created by Farooq on 8/19/2016.
 */
public abstract class StopwatchState {

    private final IView _view;

    StopwatchState(IView view) {
        _view = view;
    }

    protected IView getView() {
        return _view;
    }

    public abstract void pressStart();

    public abstract void pressStop();

    public abstract void pressReset();
}
