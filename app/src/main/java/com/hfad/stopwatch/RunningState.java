package com.hfad.stopwatch;

/**
 * Created by Farooq on 8/19/2016.
 */
public final class RunningState extends StopwatchState {

    public RunningState(IView view) {
        super(view);
    }

    @Override
    public void pressStart() {
        //Does nothing.
    }

    @Override
    public void pressStop() {
        getView().pauseTimer();
        getView().setStartButtonState(true);
        getView().setStopButtonState(false);
        getView().setResetButtonState(true);

        //When running, pressing stop pauses the timer.
        getView().setState(getView().getPausedState());

    }

    @Override
    public void pressReset() {
        //Does nothing.
    }
}
