package com.hfad.stopwatch;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

public class StopwatchActivity extends AppCompatActivity implements IView {

    private final StopwatchState _stoppedState = new StoppedState(this);
    private final StopwatchState _runningState = new RunningState(this);
    private final StopwatchState _pausedState = new PausedState(this);
    private long _pauseTime;
    private Chronometer _chronometer;
    private InstanceStateManager _instanceStateManager;
    private StopwatchState _currentState;

    private static long getElapsedTime() {
        return SystemClock.elapsedRealtime();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);

        _instanceStateManager = new InstanceStateManager(this);
        _chronometer = (Chronometer) findViewById(R.id.chrono);

        if (savedInstanceState != null) {
            _instanceStateManager.restoreInstanceState(savedInstanceState);
        } else {
            _currentState = _stoppedState;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        _instanceStateManager.saveInstanceState(outState);
    }

    public void onClickStart(View view) {
        _currentState.pressStart();
    }

    public void onClickStop(View view) {
        _currentState.pressStop();
    }

    public void onClickReset(View view) {
        _currentState.pressReset();
    }

    @Override
    public void setState(StopwatchState state) {
        _currentState = state;
    }

    @Override
    public StopwatchState getRunningState() {
        return _runningState;
    }

    @Override
    public StopwatchState getStoppedState() {
        return _stoppedState;
    }

    @Override
    public StopwatchState getPausedState() {
        return _pausedState;
    }

    @Override
    public void startTimer() {
        _chronometer.setBase(getElapsedTime());
        _chronometer.start();
    }

    @Override
    public void stopTimer() {

        if (_currentState.getClass() == PausedState.class) {
            _chronometer.setBase(getElapsedTime());
            _pauseTime = 0;
        }

        _chronometer.stop();
    }

    @Override
    public void pauseTimer() {
        _pauseTime = _chronometer.getBase() - getElapsedTime();
        _chronometer.stop();
    }

    @Override
    public void resumeTimer() {
        _chronometer.setBase(getElapsedTime() + _pauseTime);
        _chronometer.start();
    }

    @Override
    public long getPauseTime() {
        return _pauseTime;
    }

    @Override
    public void setPauseTime(long value) {
        _pauseTime = value;
    }

    @Override
    public boolean getStartButtonState() {
        return getButtonState(R.id.start_button);
    }

    @Override
    public void setStartButtonState(boolean enabled) {
        setButtonState(R.id.start_button, enabled);
    }

    @Override
    public boolean getStopButtonState() {
        return getButtonState(R.id.stop_button);
    }

    @Override
    public void setStopButtonState(boolean enabled) {
        setButtonState(R.id.stop_button, enabled);
    }

    @Override
    public boolean getResetButtonState() {
        return getButtonState(R.id.reset_button);
    }

    @Override
    public void setResetButtonState(boolean enabled) {
        setButtonState(R.id.reset_button, enabled);
    }

    @Override
    public StopwatchState getCurrentState() {
        return _currentState;
    }

    private void setButtonState(int id, boolean enabled) {
        Button button = (Button) findViewById(id);
        button.setEnabled(enabled);
    }

    private boolean getButtonState(int id) {
        Button button = (Button) findViewById(id);
        return button.isEnabled();
    }

    private String getCurrentStateClassName() {
        return _currentState.getClass().getName();
    }
}
