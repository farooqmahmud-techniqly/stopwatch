package com.hfad.stopwatch;

/**
 * Created by Farooq on 8/19/2016.
 */
public final class PausedState extends StopwatchState {

    public PausedState(IView view) {
        super(view);
    }

    @Override
    public void pressStart() {
        //When paused, pressing start resumes the timer.
        getView().resumeTimer();
        getView().setStartButtonState(false);
        getView().setStopButtonState(true);
        getView().setResetButtonState(false);
        getView().setState(getView().getRunningState());
    }

    @Override
    public void pressStop() {
        //Does nothing.
    }

    @Override
    public void pressReset() {
        //When paused, pressing stop resets the timer.
        getView().stopTimer();
        getView().setStartButtonState(true);
        getView().setStopButtonState(false);
        getView().setResetButtonState(false);
        getView().setState(getView().getStoppedState());
    }
}
