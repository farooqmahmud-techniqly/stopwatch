package com.hfad.stopwatch;

/**
 * Created by Farooq on 8/19/2016.
 */
public final class StoppedState extends StopwatchState {

    public StoppedState(IView view) {
        super(view);
    }

    @Override
    public void pressStart() {
        getView().startTimer();
        getView().setStopButtonState(true);
        getView().setStartButtonState(false);
        getView().setResetButtonState(false);
        //If stopped, pressing start resumes the timer.
        getView().setState(getView().getRunningState());

    }

    @Override
    public void pressStop() {
        //If stopped, does nothing.
    }

    @Override
    public void pressReset() {
        //If stopped, does nothing.
    }
}
