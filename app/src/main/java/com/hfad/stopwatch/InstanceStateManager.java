package com.hfad.stopwatch;

import android.os.Bundle;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Farooq on 8/19/2016.
 */
final class InstanceStateManager {
    private final IView _view;

    InstanceStateManager(IView view) {
        _view = view;
    }

    void restoreInstanceState(Bundle bundle) {

        _view.setPauseTime(bundle.getLong("pauseTime"));

        _view.setStartButtonState(bundle.getBoolean("startButtonEnabled"));
        _view.setStopButtonState(bundle.getBoolean("stopButtonEnabled"));
        _view.setResetButtonState(bundle.getBoolean("resetButtonEnabled"));

        String currentStateClassName = bundle.getString("currentState");

        try {
            Class state = Class.forName(currentStateClassName);
            Constructor constructor = state.getConstructor(IView.class);
            Object invoker = constructor.newInstance(_view);
            _view.setState((StopwatchState) invoker);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        _view.resumeTimer();
    }

    void saveInstanceState(Bundle bundle) {
        _view.pauseTimer();
        bundle.putLong("pauseTime", _view.getPauseTime());
        StopwatchState currentState = _view.getCurrentState();
        bundle.putString("currentState", currentState.getClass().getName());
        bundle.putBoolean("startButtonEnabled", _view.getStartButtonState());
        bundle.putBoolean("stopButtonEnabled", _view.getStopButtonState());
        bundle.putBoolean("resetButtonEnabled", _view.getResetButtonState());
    }
}
