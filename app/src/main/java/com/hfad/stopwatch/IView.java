package com.hfad.stopwatch;

/**
 * Created by Farooq on 8/19/2016.
 */
public interface IView {
    void setState(StopwatchState state);

    StopwatchState getRunningState();

    StopwatchState getStoppedState();

    StopwatchState getPausedState();

    void startTimer();

    void stopTimer();

    void pauseTimer();

    void resumeTimer();

    long getPauseTime();

    void setPauseTime(long value);

    boolean getStartButtonState();

    void setStartButtonState(boolean enabled);

    boolean getStopButtonState();

    void setStopButtonState(boolean enabled);

    boolean getResetButtonState();

    void setResetButtonState(boolean enabled);

    StopwatchState getCurrentState();
}
